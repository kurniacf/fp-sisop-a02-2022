#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <dirent.h>
#include <time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <sys/stat.h>

#define PORT 80 // localhost
#define BUFSIZE 1024 // buffer bytes = 1024

int data_check(char data[], char file_name[])
{
	// get file path
	char file_path[BUFSIZE];
	sprintf(file_path, "databases/client_database/%s", file_name);
	
	// open file
	FILE *user_file;
	user_file = fopen(file_path, "r");
	if(user_file == NULL)
	{
		// if failed
		printf("Error at opening file\n");
		exit(EXIT_FAILURE);
	}
	
	char line[BUFSIZE];
	// read line
	while(fgets(line, sizeof(line), user_file) != NULL)
	{
		if(strstr(line, data) != NULL)
		{
			return 1;
		}
	}

	return 0;
}

int database_check(char database_name[])
{
	if(database_name[0] == '\0')
	{
		// if failed
		printf("No database found\n");
		return 0;
	}
	
	DIR *database_dir;
	struct dirent *database_ent;
	database_dir = opendir("databases");
	while((database_ent = readdir(database_dir)) != NULL)
	{
		if(strcmp(database_name, database_ent->d_name) == 0)
		{
			return 1;
		}
	}
	
	return 0;
}

int command_len(char command[])
{
	return strlen(command)-2;
}

int main(int argc, char const *argv[])
{
	int server_socket, new_socket, valread;
	struct sockaddr_in server_address;
	int address_length = sizeof(server_address);
	char buffer[BUFSIZE] = {0};
	// int opt = 1;
	// char *test = "Server test\n";

	if((server_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0)
	{
		printf("Socket failed\n");
		exit(EXIT_FAILURE);
	}
	
	if(setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &buffer, sizeof(buffer)))
	{
		printf("Setsockopt failed\n");
		exit(EXIT_FAILURE);
	}

	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = INADDR_ANY;
	server_address.sin_port = htons(PORT);
	
	// bind server
	if(bind(server_socket, (struct sockaddr *)&server_address, sizeof(server_address)) < 0)
	{
		printf("Bind failed\n");
		exit(EXIT_FAILURE);
	}
	
	// stream socket so client connection request can be accepted
	// n connection request queue can be created
	if(listen(server_socket, 3) < 0)
	{
		printf("Listen failed\n");
		exit(EXIT_FAILURE);
	}

	// accept incoming request to socket
	if((new_socket = accept(server_socket, (struct sockaddr *)&server_address, (socklen_t *)&address_length)) < 0)
	{
		printf("Accept failed\n");
		exit(EXIT_FAILURE);
	}
	
	char *path = "/home/kurniacf/fp-sisop-A02-2022/Server";
	int root;
	char auth_user[BUFSIZE] = {0};
	char username[BUFSIZE] = {0};

	// recieve message
	recv(new_socket, &root, sizeof(root), 0);
	// authenticate user
	if(!root)
	{
		char auth_response[BUFSIZE] = {0};
		valread = read(new_socket, auth_user, BUFSIZE);
		
		int check = data_check(auth_user,  "client_account.txt");
				
		int j = 0;
		for(int i = 0; i < strlen(auth_user); i++)
		{
			if(auth_user[i] == ',')
			{
				break;
			}
			username[j] = auth_user[i];
			j++;
		}
	
		if(check)
		{
			sprintf(auth_response, "Authentication success\n");
		}
		else
		{
			sprintf(auth_response, "Account is not found\n");
		}
		// send or transmit message
		send(new_socket, auth_response, sizeof(auth_response), 0);
	}
	
	// creating command for database
	if(root)
	{
		strcpy(username, "root");
	}
	
	char used_database[BUFSIZE];
	used_database[0] = '\0';

	while(1)
	{
		time_t t = time(NULL);
		struct tm tm;
        tm = *localtime(&t);
		char command[BUFSIZE] = {0};
		char response[BUFSIZE] = {0};
		char log[BUFSIZE] = {0};
			
		valread = read(new_socket, command, BUFSIZE);
		sprintf(log, "%d-%02d-%02d %02d:%02d:%02d:%s:%s", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, username, command);
        create_log(log);
        printf("%s", command);		
			
		if(strcmp(command, "CREATE USER") == 0)
		{
				if(root)
			{
				char user[10] = {0};
				char password[10] = {0};
				int j = 0, k = 0;
				
				for(int i = 12; i < strlen(command); i++)
				{
					if(command[i] == 32)
					{
						while(1)
						{
							i++;
							if(command[i] == 32 && command[i-1] == 'Y' && command[i-2] == 'B')
							{
								i++;
								user[j] = ',';
								j++;
								break;
							}
						}
					}
				}
				
				char user_file_path[100];
				sprintf(user_file_path, "databases/client_database/client_account.txt");
				
				FILE *user_file;
				user_file = fopen(user_file_path, "a");
				if(user_file == NULL)
				{
					printf("Error at opening user file\n");
					return EXIT_FAILURE;
				}

				fputs(user, user_file);
				fputs("\n", user_file);
				fflush(user_file);
				fclose(user_file);
				sprintf(response, "[CREATE USER] success\n");
			
			}
			else
			{
				sprintf(response, "[PERMISSION DENIED] cannot create user\n");
			}
				
			send(new_socket, response, sizeof(response), 0);
		}

		else if(strcmp(command, "USE") == 0)
		{
			char database_name[BUFSIZE] = {0};
			int space_count = 0, database_count = 0;
			
			// get the first letter of database name
			for(int i = strlen(command); i > 0; i--)
			{
				if(command[i] == ' ')
				{
					space_count = i;
					break;
				}
			}
	
			// get the name
			for(int j = space_count + 1; j < command_len(command); j++)
			{
				database_name[database_count] = command[j];
				database_count++;
			}
				
			// check if database exist n user has access permission
			if(database_check(database_name))
			{
				int permitted = 0;

				if(!root)
				{
					char username[BUFSIZE] = {0};
					char permission[BUFSIZE];

					for(int i = 0; i < strlen(auth_user); i++)
					{
						if(auth_user[i] == ',')
						{
							break;
						}
							
						username[i] = auth_user[i];
					}
					
					sprintf(permission, "%s, %s", database_name, username);
					if(data_check(permission, "access_account.txt")) permitted = 1;
				} 

				if(permitted || root)
				{
					// record database name as used database 
					used_database[0] = '\0';
					strcpy(used_database, database_name);
					sprintf(response, "[USE] Database %s used successfully\n", used_database);
				}
				else
				{
					sprintf(response, "[USE] Database %s authentication failed\n", used_database);
				}
				
				send(new_socket, response, sizeof(response), 0);
			}
		
		else if(strcmp(command, "GRANT PERMISSION") == 0)
		{
			if(root)
			{
				char delimiter[] = " ";
				char *ptr = strtok(command, delimiter);
				char access[BUFSIZE] = {0};
				int pos = 0;
			
				while(ptr != NULL)
				{
					if(pos == 2)
					{
						strcat(access, ptr);
						strcat(access, ",");
					}
					
					if(pos == 4)
					{
						strcat(access, ptr);
			
					}
				
					ptr = strtok(NULL, delimiter);
					pos++;
				}

				FILE *file_access;
				file_access = fopen("databases/client_database/access_account.txt", "a");
				if(file_access == NULL)
				{
					printf("Failed on opening file\n");
					return EXIT_FAILURE;
				}

				fputs(access, file_access);
				fputs("\n", file_access);
				fflush(file_access);
				fclose(file_access);
				sprintf(response, "[PERMISSION GRANTED] can access the file\n");
			}
			else
			{
				sprintf(response, "[PERMISSION DENIED] cannot access the file\n");
			}
			
			send(new_socket, response, sizeof(response), 0);
		}

		else if(strcmp(command, "CREATE DATABASE") == 0)
		{
			char database_name[BUFSIZE] = {0};
			char database_path[BUFSIZE] = {0};
			int space_counter = 0, database_count = 0;
			
			// get the first letter of database name
			for(int i = strlen(command); i > 0; i--)
			{
				if(command[i] == ' ')
				{
					space_counter = i;
					break;
				}
			}
	
			// get the name
			for(int j = space_count + 1; j < command_len(command); j++)
			{
				database_name[database_count] = command[j];
				database_count++;
			}

			// create database path
			sprintf(database_path, "databases/%s", database_name);

			// create database directory
			int create_dir_failed = mkdir(database_path, 0777);
			
			if(create_dir_failed)
				printf("Failed on creating directory\n");
				return EXIT_FAILURE;
		}

			// add user permission
			if(!root)
			{
				char permission[BUFSIZE] = {0};
				char username[BUFSIZE] = {0};

				for(int i = 0; i < strlen(auth_user); i++)
				{
					if(auth_user[i] == ',')
					{
						break;
					}
				
					username[i] = auth_user[i];
				}

				sprintf(permission, "%s, %s", database_name, username);
				
				FILE *file_access;
				file_access = fopen("databases/client_database/access_account.txt", "a");
				if(file_access == NULL)
				{
					printf("Failed on opening file\n");
					return EXIT_FAILURE;
				}

				fprintf(file_access, "%s\n", permission);
				fflush(file_access);
				fclose(file_access);
			}

			sprintf(response, "[CREATE DATABASE] Database %s created successfully\n", database_name);
			send(new_socket, response, sizeof(response), 0);
		}

		else if(strcmp(command, "CREATE TABLE") == 0)	
		{
			if(used_database[0] == '\0')
			{
				sprintf(response, "No database is in use\n");
			}
			else
			{
				char table_name[BUFSIZE] = {0};
				char table_detail[BUFSIZE] = {0};
				int j = 0, k = 0, space = 0;
				
				for(int i = 0; i < command_len(command); i++)
				{
					if(command[i] == '(' || command[i] == ')')
                    		{
						continue;
					}
                    		if(command[i] == 32)
                   		{
                        		space++;
                    		}
                    		if(space == 2 && command[i] != 32)
                    		{
                        		table_name[j] = command[i];
                        		j++;
                    		}
                   		if(space >= 3)
                    		{
                        		if((space == 3 && command[i] == 32) || (command[i] == 32 && command[i-1] == ','))
						{
                            			continue;
						}
                        		table_detail[k] = command[i]; 
                        		k++;
                    		}
				}

				char table_file_path[BUFSIZE] = {0};
				sprintf(table_file_path, "databases/%s/%s.txt", used_database, table_name);
				
				FILE *table_file;
				table_file = fopen(table_file_path, "a");
				if(table_file == NULL)
				{
					printf("Failed on opening table file\n");
					return EXIT_FAILURE;
				}
				fputs(table_detail, table_file);
				fputs("\n\n", table_file);
				fflush(table_file);
				fclose(table_file);

				sprintf(response, "[CREATE TABLE] success\n");
			}
		
			send(new_socket, response, sizeof(response), 0);
		}

		else if(strcmp(command, "DROP") == 0)
		{
			char username[BUFSIZE] = {0};
			char permission[BUFSIZE] = {0};
			char object[BUFSIZE] = {0};
			char object_path[BUFSIZE] = {0};
			char object_type[BUFSIZE] = {0};
			char *delimiter = " ,;\n";
			char *tok = strtok(command, delimiter);
			int res = -1;

			for(int i = 0; i < strlen(auth_user); i++)
			{
				if(auth_user[i] == ',')
				{
					break;
				}

				username[i] = auth_user[i];
			}
			
			tok = strtok(NULL, delimiter);
			strcpy(object_type, tok);
			if(strcmp(object_type, "DATABASE") == 0)
			{
				tok = strtok(NULL, delimiter);
				strcpy(object, tok);
				sprintf(permission, "%s, %s", object, username);
			}
			else if(strcmp(object_type, "TABLE"))
			{
				tok = strtok(NULL, delimiter);
				sprintf(object, "%s/%s.txt", used_database, tok);
				sprintf(permission, "%s, %s", used_database, username);
			}
			
			sprintf(object_path, "databases/%s", object);

			if(data_check(permission, "access_account.txt") && used_database[0] != '\0')
			{
				res = remove(object_path);
			}

			if(used_database[0] == '\0')
			{
				sprintf(response, "No database is in use\n");
			}

			if(res == 0)
			{
				sprintf(response, "[DROP] success\n");
			}
			
			else if(errno == ENOTEMPTY || errno == ENOENT)
			{
				sprintf(response, "[DROP] failed: table not empty or table not exist\n");
			}

			else 
			{
				sprintf(response, "[DROP] failed\n");
			}

			send(new_socket, response, sizeof(response), 0);
		}

		else if(strcmp(command, "INSERT INTO") == 0)	
        {
            if(used_database[0] = '\0')
			{
				sprintf(response, "No database is in use\n");
			}
            else {
                char table_name[BUFSIZE] = {0};
				char column_val[BUFSIZE] = {0};
				int space_count = 0, table_name_count = 0, column_val_counter = 0;

                for(int i = 0; i < command_len(command); i++)
                {
                    if(command[i] == ' ')
                    {
                        space_count++;
                    }

                    if(command[i] == '(' || command[i] == ')' || command[i] == '\0') 
                    {
                        continue;
                    }
                    if(space_count == 2 && command[i] != ' ')
                    {
                        table_name[table_name_count] = command[i];
						table_name_count++;
                    }
                    if(space_count >= 3)
                    {
                        if((command[i] == ' ' && space_count == 3) || (command[i] == ' ' && command[i-1] == ','))
                        {
                            continue;
                        }
                    }
                    column_val[column_val_counter] = command[i];
					column_val_counter++;
                }

                char table_path[BUFSIZE] = {0};
				sprintf(table_path, "databases/%s/%s.txt", used_database, table_name);
				
				FILE *table_file;
				table_file = fopen((table_path), "r");
				if(table_file == NULL)
				{
					sprintf(response, "No table directory %s found\n", table_path);
				}
                else
				{
					table_file = fopen(table_path, "a");
					fprintf(table_file, "%s\n", column_val);
					fflush(table_file);
					fclose(table_file);

					sprintf(response, "[INSERT INTO] success\n");
				}	
                send(new_socket, response, sizeof(response), 0);
            }
        }
		else if(strcmp(command, "DELETE") == 0)
		{
			if(used_database[0] == '\0')
			{
				sprintf(response, "No database is in use\n");
			}
			else
			{
				char table_name[BUFSIZE] = {0};
				int space_count = 0, table_name_count = 0;

				// get the first letter of table
				for(int i = strlen(command); i>0; i--)
				{
					if(command[i] == ' ')
					{
						space_count = i;
						break;
					}
				}

				// get the name
				for(int j = space_count + 1; j < command_len(command); j++)
				{
					table_name[table_name_count] = command[j];
					table_name_count++;
				}
			
				char table_path[BUFSIZE] = {0};
				sprintf(table_path, "databases/%s/%s.txt", used_database, table_name);
				
				FILE *table_file;
				table_file = fopen(table_path, "r");
				if(table_file == NULL)
				{
					sprintf(response, "No table directory %s found\n", table_path);
				}
				else
				{
					// replace with a new file
					char table_content[BUFSIZE];
					fgets(table_content, sizeof(table_content), table_file);
					remove(table_path);
					table_file = fopen(table_path, "a");
					fprintf(table_file, "%s\n", table_content);
					
					fflush(table_file);
					fclose(table_file);
					sprintf(response, "[DELETE] success\n");
				}
			}
	
			send(new_socket, response, sizeof(response), 0);
		}
	
		else if(strcmp(command, "UPDATE") == 0)
		{

		}

		else if(strcmp(command, "SELECT") == 0)
		{

		}

		// else if(strcmp(command, "WHERE") == 0)

		else
		{
			break;
		}	
		
	}	
		
	// create user, use, grant permission,
	// create database, create table, drop,
	// insert into, delete, update, select, where?

	return 0;
}
