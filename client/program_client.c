#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>

#define PORT 80 // localhost
#define BUFSIZE 1024 // buffer bytes = 1024

int main(int arg, char const *argv[])
{
	// server binding
	int server_socket, valread;
	struct sockaddr_in server_address;
	
	if ((server_socket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		printf("Socket failed\n");
		exit;
	}

	memset(&server_address, '0', sizeof(server_address));
	server_address.sin_family = AF_INET;
	server_address.sin_port = htons(PORT); // returns port number in host byte order to in network byte order
	
	// converts string "127.0.0.1" (localhost IP Address) into a network address structure in AF_INET address
	// and copies it to sin_addr
	if (inet_pton(AF_INET, "127.0.0.1", &server_address.sin_addr) <= 0)
	{
		printf("\nInvalid address\n");
		exit;
	}

	// connect to server
	if (connect(server_socket, (struct sockaddr *)&server_address, sizeof(server_address)) < 0)
	{
		printf("\nFailed to connect\n");
		exit;
	}

	// check if user is in root
	int root;
	if (getuid() != 0) 
	{
		// send message on a socket while connected
		send(server_socket, &root, sizeof(root), 0);
		char user[BUFSIZE] = {0};
		char auth_response[BUFSIZE] = {0};
		sprintf(user, "%s,%s\n", argv[2], argv[4]);
		send(server_socket, user, strlen(user), 0);
		valread = read(server_socket, auth_response, BUFSIZE);
		printf("%s\n", auth_response);
	}
	else
	{
		root = 1;
		send(server_socket, &root, sizeof(root), 0);
	}
	
	// get command request n response
	while(1)
	{
		char command[BUFSIZE] = {0};
		char response[BUFSIZE] = {0};
		fgets(command, sizeof(command), stdin);
		send(server_socket, command, strlen(command), 0);
		valread = read(server_socket, response, BUFSIZE);
		printf("%s\n", response);

		if(strcmp(command, "EXIT") == 0)
		{
			break;
		}
	}

	return 0;
}